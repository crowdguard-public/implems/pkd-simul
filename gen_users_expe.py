import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate a set users for the crowdpriv experiments for various methods'
    )

    parser.add_argument('--repro', '-r', type=int, required=True, help="Number of reproductions.")
    parser.add_argument('--dims', '-d', type=int, required=True, help="Number of dimensions.")
    parser.add_argument('--nusers', '-n', type=int, required=True, help='Number of users per set')
    parser.add_argument('--out', '-o', type=str, required=True, help='Output directory')
    parser.add_argument('--unif', action='store_const', const=True, default=False, help='Use the UNIF generation mode')
    parser.add_argument('--onespe', action='store_const', const=True, default=False, help='Use the ONESPE generation mode')
    parser.add_argument('--csv', type=str, help='Use the CSV generation mode')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    modes = []
    if args.unif:
        modes.append('unif')
    if args.onespe:
        modes.append('onespe')
    if args.csv:
        modes.append('csv')

    for i in range(args.repro):
        for mode in modes:
            cmd = 'python3 ./gen_users.py ' \
                  '--nusers ' + str(args.nusers) + ' --dimensions ' + str(args.dims)
            cmd += ' --' + mode
            if mode == 'csv':
                cmd += ' ' + args.csv

            out = os.path.join(args.out, mode + '_' + str(i) + '.csv')
            cmd += ' --out ' + out

            os.system(cmd)
