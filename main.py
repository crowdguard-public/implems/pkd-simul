import dim
import users
import eps
import t
import kdtree
import count
import median
import sys
import consistency
import evaluation
import csvimport
import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate an experiment'
    )

    parser.add_argument('--short', action='store_const', const=True, default=False,
                        help='Condensed (as a CSV line) results)')
    parser.add_argument('--dims', '-d', type=int, required=True, help='Number of dimensions')
    parser.add_argument('--nusers', '-n', type=int, required=True, help='Number of users')
    parser.add_argument('--depth', type=int, required=True, help='Tree depth')
    parser.add_argument('--bins', type=int, required=True, help='Number of bins')
    parser.add_argument('--epsilon', type=float, required=True, help='Epsilon')
    parser.add_argument('--eps', type=float, required=True, help='EPS ratio count')
    parser.add_argument('--printtree', action='store_const', const=True, default=False,
                        help='Print the tree')
    parser.add_argument('--coalition', type=int, required=True, help='Coalition')
    parser.add_argument('--ntasks', type=int, required=True, help='Number of tasks')
    parser.add_argument('--wdump', type=str, help='Worker dump file')

    pug = parser.add_mutually_exclusive_group(required=True)
    pug.add_argument('--users_unif', action='store_const', const=True, default=False,
                     help='Generate users with the UNIF generator')
    pug.add_argument('--users_onespe', action='store_const', const=True, default=False,
                     help='Genreate users with the ONESPE generator')
    pug.add_argument('--users_csv', type=str, help='Import users from a CSV file (result of the STACK generator)')

    ptg = parser.add_mutually_exclusive_group(required=True)
    ptg.add_argument('--tasks_unif', action='store_const', const=True, default=False,
                     help='Generate tasks with the UNIF generator')
    ptg.add_argument('--tasks_onespe', action='store_const', const=True, default=False,
                     help='Generate tasks with the ONESPE generator')
    ptg.add_argument('--tasks_tree', type=int, help='Generate tasks from tree node (0: root, depth -1: leaves) '
                                                    'take ntasks random nodes')

    psc = parser.add_mutually_exclusive_group(required=True)
    psc.add_argument('--sc_noisy', action='store_const', const=True, default=False,
                     help='Noisy count strategy')
    psc.add_argument('--sc_precise', action='store_const', const=True, default=False,
                     help='Precise count strategy')

    pmc = parser.add_mutually_exclusive_group(required=True)
    pmc.add_argument('--sm_noisy', action='store_const', const=True, default=False,
                     help='Noisy median strategy')
    pmc.add_argument('--sm_hist', action='store_const', const=True, default=False,
                     help='Histogram (without noise) strategy)'
                     )
    pmc.add_argument('--sm_precise', action='store_const', const=True, default=False,
                     help='Precise median strategy')

    return parser.parse_args()


def kd_print(args):
    tree = kdtree.kdtree(user_set,
                         prof_max,
                         bins,
                         epsilon,
                         node,
                         prof_max,
                         eps.quad_count,
                         eps.uniform_median,
                         dim.next_unused,
                         used_median,
                         used_count,
                         ratio_count,
                         2,
                         coalition
                         )

    consist_tree = consistency.compute(tree, prof_max, epsilon, "quad", eps_init, 2, ratio_count)
    if args.short:
        mini_results = arguments_recus(args)
    else:
        """
        mini_results = ""
        if printer:
            consist_tree.printTree()
        for i in range(len(sys.argv)-1):
            print(arguments[i]+" : "+sys.argv[i+1])
        for arg in arg_bonus:
            print(arg[0]+" : "+arg[1])
        """
        print("TODO")
    return [consist_tree, mini_results]


def arguments_recus(args):
    res = str(args.dims) + ';' +\
           str(args.nusers) + ';' +\
           str(args.depth) + ';' +\
           str(args.bins) + ';' +\
           str(args.epsilon) + ';' +\
           str(args.eps) + ';'

    if args.users_unif:
        res += 'unif'
    elif args.users_onespe:
        res += 'onespe'
    elif args.users_csv:
        res += 'csv'
    res += ';'

    res += str(args.printtree) + ';' + \
           str(args.coalition) + ';' +\
           str(args.ntasks) + ';' +\
           str("TODO FORMAT TACHE") + ';' +\
           str("TODO STRAT COUNT") + ';' +\
           str("TODO STRAT MEDIAN") + ';'
    return res


# ########## MAIN ###########
args = parse_args()

dimension = args.dims
n_user = args.nusers
prof_max = args.depth
bins = args.bins
epsilon = args.epsilon
ratio_count = args.eps
printer = args.printtree
coalition = args.coalition
nb_tasks = args.ntasks


arg_bonus = []

# Count strategy
if args.sc_noisy:
    strat_count = 'noisy'
elif args.sc_precise:
    strat_count = 'precise'
used_count = count.choose(strat_count)

# Median strategy
# precise pour pas d'histogrammes
# hist pour histogrammes non bruités
# noisy_hist pour histogrammes bruités
if args.sm_noisy:
    strat_median = 'noisy_hist'
elif args.sm_hist:
    strat_median = 'hist'
elif args.sm_precise:
    strat_median = 'precise'
used_median = median.choose(strat_median)

# Workers generation
user_set = users.generate_users(args)
users.dump_users(args, user_set)

# Création du nœud initial
# eps_init = 100
eps_init = eps.quad_count(epsilon, 0, prof_max, ratio_count)
node = t.Node([[0, 1]] * dimension, n_user, user_set, eps_init, (eps_init**2) * n_user)

cons = kd_print(args)


eval_queries = evaluation.eval_global(args, nb_tasks, user_set, dimension, cons[0])


if not args.short:
    print("\nErreur relative moyenne : "+str(eval_queries[0]))
    print("Erreur moyenne (quadratique) : "+str(eval_queries[1]))
    print("Erreur moyenne (linéaire) : "+str(eval_queries[2]))
    print("Ecart type de l'erreur : "+str(eval_queries[3]))
    print("Max tasks for a leaf : "+str(eval_queries[4]))
    print("Tasks with no users (unpredicted) : "+str(eval_queries[5]))

# On a donc : moyenne quadratique et linéaire, et écart-type de l'erreur (l'erreur vaut en moyenne ça, mais des fois, c'est vachement plus élevé/c'est toujours très proche de ça)
else:
    cons[1] += str(eval_queries[0]) + ";" +\
               str(eval_queries[1]) + ";" +\
               str(eval_queries[2]) + ";" +\
               str(eval_queries[3]) + ";" +\
               str(eval_queries[4]) + ";" +\
               str(eval_queries[5])
    print(cons[1])
