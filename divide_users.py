# crée la liste des utilisateurs présents dans chaque volume


def is_in(user, volume, dim):
    return all(
        (volume[d][0] <= user[d] < volume[d][1] or
         (volume[d][1] == 1 and
          volume[d][0] <= user[d] <= volume[d][1]
          )
         )
        for d in range(dim)
    )


def divide(matrix_users, volumes, dim):
    """

    :param matrix_users: List of users
    :param volumes:
    :param dim:
    :return:
    """
    result = [[]] * len(volumes)

    if matrix_users is []:
        return result
    else:
        if len(matrix_users) == 0:  # Empty users list to divide
            # Previous code was just:
            # dim = len(matrix_users[0])
            pass

        for u in matrix_users:
            for v in range(len(volumes)):
                if is_in(u, volumes[v], dim):
                    result[v] = result[v] + [u]
                    break
        return result

