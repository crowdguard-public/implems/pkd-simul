import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import csv
import sys

if len(sys.argv) == 1:
    print("Set the workers CSV file as an argument")
    exit(-1)

path = sys.argv[1]

datas = pd.read_csv(path, header=None)
print(datas.describe())