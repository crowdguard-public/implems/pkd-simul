import pandas as pd
import seaborn as sns
import csv
import sys
from os.path import join

try:
    path = sys.argv[1]
except:
    print("Set the experiment data path.")
    exit(-42)

# Indexes of elements
DEPTH = 2
BINS = 3
EPSILON = 4
USERS = 6
ERROR = 13

########## DEPTH ##########

file = join(path, "depth.csv")
with open(file) as csvfile:
    spamreader = csv.reader(csvfile, delimiter=";")
    data = []
    for row in spamreader:
        ds = []
        ds += [int(row[DEPTH])]   # 0: depth
        ds += [int(row[BINS])]   # 1: bins
        ds += [float(row[EPSILON])]  # 2: espilon

        type = row[USERS]
        if type == 'csv':
            type = 'STACK'
        if type == 'onespe':
            type = 'ONESPE'
        if type == 'unif':
            type = 'UNIF'
        ds += [type]        # 3: type (csv, ...)
        ds += [float(row[ERROR])]  # 4: mean error

        data += [ds]

df = pd.DataFrame(data, columns=['depth', 'bins', 'epsilon', 'type', 'mean error'])

pal = {'STACK': 'blue', 'ONESPE': 'green', 'UNIF': 'orange'}
order = ['UNIF', 'ONESPE', 'STACK']
plot = sns.barplot(x='depth', y='mean error', hue='type', hue_order=order, palette=pal, data=df)
plot.get_figure().savefig(join(path, "depth.png"))
plot.clear()


########## BINS ##########

file = join(path, "bins.csv")
with open(file) as csvfile:
    spamreader = csv.reader(csvfile, delimiter=";")
    data = []
    for row in spamreader:
        ds = []
        ds += [int(row[DEPTH])]   # 0: depth
        ds += [int(row[BINS])]   # 1: bins
        ds += [float(row[EPSILON])]  # 2: espilon
        type = row[USERS]
        if type == 'csv':
            type = 'STACK'
        if type == 'onespe':
            type = 'ONESPE'
        if type == 'unif':
            type = 'UNIF'
        ds += [type]        # 3: type (csv, ...)
        ds += [float(row[ERROR])]  # 4: mean error

        data += [ds]

df = pd.DataFrame(data, columns=['depth', 'bins', 'epsilon', 'type', 'mean error'])

pal = {'STACK': 'blue', 'ONESPE': 'green', 'UNIF': 'orange'}
order = ['UNIF', 'ONESPE', 'STACK']
plot = sns.barplot(x='bins', y='mean error', hue='type', hue_order=order, palette=pal, data=df)
plot.get_figure().savefig(join(path, "bins.png"))
plot.clear()

########## EPSILON ##########

file = join(path, "epsilon.csv")
with open(file) as csvfile:
    spamreader = csv.reader(csvfile, delimiter=";")
    data = []
    for row in spamreader:
        ds = []
        ds += [int(row[DEPTH])]   # 0: depth
        ds += [int(row[BINS])]   # 1: bins
        ds += [float(row[EPSILON])]  # 2: espilon
        type = row[USERS]
        if type == 'csv':
            type = 'STACK'
        if type == 'onespe':
            type = 'ONESPE'
        if type == 'unif':
            type = 'UNIF'
        ds += [type]        # 3: type (csv, ...)
        ds += [float(row[ERROR])]  # 4: mean error

        data += [ds]

df = pd.DataFrame(data, columns=['depth', 'bins', 'epsilon', 'type', 'mean error'])

pal = {'STACK': 'blue', 'ONESPE': 'green', 'UNIF': 'orange'}
order = ['UNIF', 'ONESPE', 'STACK']
plot = sns.barplot(x='epsilon', y='mean error', hue='type', hue_order=order, data=df, palette=pal)
plot.get_figure().savefig(join(path, "epsilon.png"))
