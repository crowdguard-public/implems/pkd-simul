import numpy
from scipy.stats import nbinom

#see https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.nbinom.html#scipy.stats.nbinom pour la doc


def is_in(user, volume, dim):
    return all(
        (volume[d][0] <= user[d] < volume[d][1] or
         (volume[d][1] == 1 and
          volume[d][0] <= user[d] <= volume[d][1]
          )
         )
        for d in range(dim)
    )


def precise(epsilon, volume, matrix_user, coalition):
    dim = len(volume)
    return sum(is_in(user, volume, dim) for user in matrix_user)


def noisy(epsilon, volume, matrix_user, coalition):
    n_user = len(matrix_user)
    p = 1 - numpy.exp(-epsilon)
    list_noise_plus = nbinom.rvs(1/(n_user-coalition), p, 0, n_user)
    geom_plus = sum(list_noise_plus)
    list_noise_moins = nbinom.rvs(1/(n_user-coalition), p, 0, n_user)
    geom_moins = sum(list_noise_moins)
    noise = geom_plus - geom_moins
    return precise(epsilon, volume, matrix_user, coalition) + noise


def choose(entree):
    if entree == "noisy":
        return noisy
    elif entree == "precise":
        return precise
    else:
        print("stratégie de comptage inconnue. Les stratégies possibles sont : \"noisy\" et \"precise\"")



