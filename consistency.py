import t
import eps

#Les feuilles sont déjà faites dans le calcul du kd-tree
def calcul_Z(tree):
    children = tree.getChildren()
    sum_z = 0
    for i in range(len(children)):
        if (children[i].getNode().getZ())==-1:
            children[i] = calcul_Z(children[i])
        sum_z = sum_z + children[i].getNode().getZ()
    tree.getNode().setZ(sum_z)
    return tree


def table(h, epsilon, budget, init,fanout, ratio_count):
    if ratio_count == 0:
        if budget == "quad":
            #1 car on calcule le compte avec le budget d'avant
            t = [eps.quad_median(epsilon, 1, h, ratio_count)**2]
            #t = [2**(h/3)*constante]
            for i in range(h-1):
            #+2 car on calcule le compte avec le budget d'avant (et +1 initial parce que le premier rang est déjà fait)
                t = t + [t[i] + fanout**(i+1) * eps.quad_median(epsilon, (i+2), h, ratio_count)**2]
            t = t + [t[-1] + (init**2*(fanout**h))]
            return t
        else:
            t = [(epsilon/h)**2]
            for i in range(h-1):
                t = t + [t[i] + (fanout**(i+1))*((e/h)**2)]
            t = t + [t[-1] + init**2*(fanout**h)]
            return t
    else:
        if budget == "unif":
            e = epsilon*ratio_count
            t = [(e/h)**2]
            for i in range(h-1):
                t = t + [t[i] + (fanout**(i+1))*((e/h)**2)]
            t = t + [t[-1] + init**2*(fanout**h)]
            return t
        else:
            #1 car on calcule le compte avec le budget d'avant
            t = [eps.quad_count(epsilon, 1, h, ratio_count)**2]
            #t = [2**(h/3)*constante]
            for i in range(h-1):
            #+2 car on calcule le compte avec le budget d'avant (et +1 initial parce que le premier rang est déjà fait)
                t = t + [t[i] + fanout**(i+1) * eps.quad_count(epsilon, (i+2), h, ratio_count)**2]
            t = t + [t[-1] + (init**2*(fanout**h))]
            return t

def calcul_F_beta(tree, root, beta_eps, table, prof_act, fanout):
    node = tree.getNode()
    children = tree.getChildren()
#    tree_l = tree.getLeft()
#    tree_r = tree.getRight()
    z = node.getZ()
    e = node.getEpsilon()
    if root:
        node.setF(0)
        new_beta = z/table[prof_act]
        node.setBeta(new_beta)
        for i in range(fanout):
            calcul_F_beta(children[i], False, new_beta*(e**2), table, prof_act -1, fanout)
#        calcul_F_beta(tree_l, False, new_beta*(e**2), table, prof_act -1)
#        calcul_F_beta(tree_r, False, new_beta*(e**2), table, prof_act -1)
    elif tree.isNull():
        node.setF(beta_eps)
        new_beta = (node.getZ() - (fanout**prof_act) * beta_eps)/table[prof_act]
        node.setBeta(new_beta)
    else:
        node.setF(beta_eps)
        new_beta = (node.getZ() - (fanout**prof_act) * beta_eps)/table[prof_act]
        node.setBeta(new_beta)
        for i in range(fanout):
            calcul_F_beta(children[i], False, beta_eps + new_beta*(e**2), table, prof_act -1, fanout)
#        calcul_F_beta(tree_r, False, beta_eps + new_beta*(e**2), table, prof_act -1)
    return tree


def compute(tree, prof_max, epsilon, strat_epsilon, eps_init, fanout, ratio_count):
    t = table(prof_max, epsilon, strat_epsilon, eps_init, fanout, ratio_count)
    with_Z = calcul_Z(tree)
    return calcul_F_beta(with_Z, True, 0, t, prof_max, fanout)




