import t
import divide_users


def kdtree(matrix_user, prof_max, bins, epsilon, node, prof_act , strat_eps_count, strat_eps_med, strat_dim, calc_med, calc_count, ratio_count, fanout, coalition):

    if (prof_act == 0):
        node.setZ(node.getAlpha())
        return t.Tree(node, [], -1, [])

    else:
        users_inside = node.getPeople()
        #dimension à diviser
        v = node.getVolume()
        new_dim = strat_dim(v, prof_act, prof_max)

        #MEDIAN
        #Budget pour la division puis division avec la median
        e_med = strat_eps_med(epsilon, prof_act, prof_max, ratio_count)
        #if (e_med < 0) & (e_med != -1):
        #    print("erreur, epsilon med < 0")
        med = calc_med(e_med, new_dim, v, matrix_user, bins, fanout, coalition)
        old_l = med[0][0]
        old_r = med[0][fanout]

        #Création des volumes équivalents
        #"v[:]" pour créer une nouvelle instance indépendante

        new_vs = []
        eps_count = fanout * [e_med]
        count_approx = [med[1]]*fanout

        alpha = node.getAlpha()
        alpha_children = []
        node_children = []
        tree_table = []

        for i in range(fanout):
            new_vs = new_vs + [v[:]]
            new_vs[i][new_dim] = [med[0][i], med[0][i+1]]

        pop_inside = divide_users.divide(users_inside, new_vs, new_dim)
        #v_left = v[:]
        #v_right = v[:]
        #v_left[new_dim] = (old_l, med[0])
        #v_right[new_dim] = (med[0], old_r)

        for i in range(fanout):

        #COUNT
        #Calcul des counts des nœuds enfants
        #Si on fait des counts indépendants des médianes
            if ratio_count != 0:
                eps_count[i] = strat_eps_count(epsilon, prof_act, prof_max,ratio_count)
                count_approx[i] = calc_count(eps_count[i], new_vs[i], matrix_user, coalition)
         #   if (eps_count[i] < 0) & (eps_count[i] !=-1):
         #       print("erreur, epsilon count < 0")

        #creation des nœuds
            alpha_children = alpha_children + [alpha + (eps_count[i]**2*count_approx[i])]
            node_children = node_children + [t.Node(new_vs[i], count_approx[i], pop_inside[i],  eps_count[i], alpha_children[i])]
        #application récursive sur les nouveaux nœuds
            tree_table = tree_table + [kdtree(matrix_user, prof_max, bins, epsilon, node_children[i], (prof_act-1) , strat_eps_count, strat_eps_med, strat_dim, calc_med, calc_count, ratio_count, fanout, coalition)]

        #return
        return t.Tree(node, tree_table, new_dim, med[0][1:-1])



