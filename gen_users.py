import argparse
import csv
import random
from scipy.stats import uniform


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate users for the crowdpriv experiments'
    )

    parser.add_argument('--nusers', '-n', required=True, type=int, help='Number of users.')
    parser.add_argument('--dimensions', '-d', required=True, type=int, help='Number of dimensions in the workers set.')
    parser.add_argument('--out', '-f', required=True, type=str, help='Output file.')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--unif', '-u', action='store_const', const=True, help='Generate NUSERS with the UNIF generator.')
    group.add_argument('--onespe', '-o', action='store_const', const=True, help='Generate NUSERS with the ONESPE generator.')
    group.add_argument('--csv', '-c', type=str, help='Choose NUSERS randomly from a workers (with DIMENSIONS dimensions) CSV file')

    return parser.parse_args()


def generator_unif(nusers, dim):
    """
    Generate nusers of dim dimension
    :param nusers: Number of users
    :param dim: Number of dimensions
    :return:
    """
    workers = []
    for u in range(args.nusers):
        workers.append(uniform.rvs(0, 1, dim))
    return workers


def generator_one_spe(nusers, dim):
    workers = []
    for u in range(args.nusers):
        k = random.randint(0, args.dimensions - 1)
        worker = uniform.rvs(0, 0.5, args.dimensions)
        worker[k] = uniform.rvs(0.5, 0.5, 1)[0]
        workers.append(worker)
    return workers


def generator_csv(path, nusers, dim):
    """
        Import a workers CSV file as a array of workers
        :param path: workers CSV file path
        :param nusers: Final number of users. Taken randomly in the list if there is more than nusers
               or return the raw workers set if there is less than nusers
        :param dim : Expected number of dimensions
        :return: An array of workers (array of skills level)
        """
    # Array of workers
    workers = []
    # Current worker skills
    current = []
    # Tags mapping
    skills_map = {}
    # Tags mapping index
    skills_map_index = 0

    # Current worker csv id
    userid = None
    # Number of imported workers
    i = 0

    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile)

        for row in reader:
            user = int(row[0])
            tag = int(row[1])
            level = float(row[2])

            # New user detected, add the old one
            if userid != user:
                if userid:
                    workers.append(current)
                    i += 1
                current = [0.] * len(skills_map)
                userid = user

            # Add the skill to the user
            index = skills_map.get(tag)
            if index == None:
                if i == 0:
                    skills_map[tag] = skills_map_index
                    index = skills_map_index
                    current.append(0)
                    skills_map_index += 1
                else:
                    print("ERROR NEW TAG")
                    exit(-42)  # Not supposed to discover new tags at this point
            current[index] = level

        workers.append(current)

    if len(workers) > nusers:
        workers = random.sample(workers, nusers)

    return workers


def save_to_csv(workers, path):
    """
    Save the workers list to CSV.
    One line contains a workers, each column mean a dimension.
    :param workers: Workers list
    :param path: CSV file path
    :return:
    """
    with open(path, "w") as csvfile:
        writter = csv.writer(csvfile)

        for worker in workers:
            writter.writerow(worker)


if __name__ == '__main__':
    args = parse_args()

    if args.unif:  # UNIF generator
        workers = generator_unif(args.nusers, args.dimensions)
    if args.onespe:  # ONESPE generator
        workers = generator_one_spe(args.nusers, args.dimensions)
    if args.csv:  # CSV generator
        workers = generator_csv(args.csv, args.nusers, args.dimensions)

    save_to_csv(workers, args.out)
