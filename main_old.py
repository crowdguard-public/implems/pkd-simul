import dim
import users
import eps
import t
import kdtree
import count
import median
import sys
import consistency
import evaluation
import csvimport
import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate an experiment'
    )

    parser.add_argument('--short', action='store_const', const=True, default=False,
                        help='Condensed (as a CSV line) results)')
    parser.add_argument('--dims', '-d', type=int, required=True, help='Number of dimensions')
    parser.add_argument('--nusers', '-n', type=int, required=True, help='Number of users')
    parser.add_argument('--depth', type=int, required=True, help='Tree depth')
    parser.add_argument('--bins', type=int, required=True, help='Number of bins')
    parser.add_argument('--epsilon', type=float, required=True, help='Epsilon')
    parser.add_argument('--eps', type=float, required=True, help='EPS ratio count')
    parser.add_argument('--printtree', action='store_const', const=True, default=False,
                        help='Print the tree')
    parser.add_argument('--coalition', type=int, required=True, help='Coalition')
    parser.add_argument('--ntasks', type=int, required=True, help='Number of tasks')

    pug = parser.add_mutually_exclusive_group(required=True)
    pug.add_argument('--user-unif', action='store_const', const=True, default=False,
                     help='Generate users with the UNIF generator')
    pug.add_argument('--user-onespe', action='store_const', const=True, default=False,
                     help='Genreate users with the ONESPE generator')
    pug.add_argument('--user-csv', type=str, help='Import users from a CSV file (result of the STACK generator)')

    ptg = parser.add_mutually_exclusive_group(required=True)
    ptg.add_argument('--tasks-unif', action='store_const', const=True, default=False,
                     help='Generate tasks with the UNIF generator')
    ptg.add_argument('--tasks-onespe', action='store_const', const=True, default=False,
                     help='Generate tasks with the ONESPE generator')
    ptg.add_argument('--tasks-tree', type=int, help='Generate tasks from tree node (0: root, depth -1: leaves) '
                                                    'take ntasks random nodes')

    psc = parser.add_mutually_exclusive_group(required=True)
    psc.add_argument('--sc-noisy', action='store_const', const=True, default=False,
                     help='Noisy count strategy')
    psc.add_argument('--sc-precise', action='store_const', const=True, default=False,
                     help='Precise count strategy')

    pmc = parser.add_mutually_exclusive_group(required=True)
    pmc.add_argument('--sm-noisy', action='store_const', const=True, default=False,
                     help='Noisy median strategy')
    pmc.add_argument('--sm-precise', action='store_const', const=True, default=False,
                     help='Precise median strategy')

    return parser.parse_args()


arguments = [
        "résultats condensés",
        "nombre de dimensions",
        "nombre d'utilisateur·ices",
        "profondeur max",
        "nombre de bins de l'histogramme",
        "epsilon",
        "ratio de eps pour les counts",
        "modélisation des utilisateur·ices",  # TM
        "impression de l'arbre",
        "coalition",
        "nombre de tâches",
        "format de tâche",  # TM
        "stratégie de comptes (facultatif)",  # TM
        "stratégie de médiane (facultatif)",  # TM
        "Fichiers d'utilisateurs (facultatif, csv)"  # (in model users)
]


def help():
    print("Exemple de commande : python3 main_ter.py False 10 1000 7 10 0.1 0.7 norm_centered False 1 100 one_spe\n")
    print("Utilisation : ")
    for i in range(len(arguments)):
        print(" Argument "+str(i+1)+" : "+arguments[i])
    print("")


try:
    mini = (sys.argv[1]=="True")
except IndexError:
    help()


dimension = int(sys.argv[2])
n_user = int(sys.argv[3])
prof_max = int(sys.argv[4])
bins = int(sys.argv[5])
epsilon = float(sys.argv[6])
ratio_count = float(sys.argv[7])
user_type = sys.argv[8]
printer = (sys.argv[9]=="True")
coalition = int(sys.argv[10])
nb_tasks = int(sys.argv[11])
format_tache = sys.argv[12]

arg_bonus = []

try:
    strat_count = sys.argv[13]
except IndexError:
    strat_count = "noisy"
    arg_bonus = arg_bonus + [[arguments[12], "noisy"]]
# noisy ou precise

try:
    strat_median = sys.argv[14]
except IndexError:
    strat_median = "noisy_hist"
    arg_bonus = arg_bonus + [[arguments[13], "noisy_hist"]]
# strat_median :
# precise pour pas d'histogrammes
# hist pour histogrammes non bruités
# noisy_hist pour histogrammes bruités


used_count = count.choose(strat_count)
used_median = median.choose(strat_median)


def arguments_recus():
    args = ""
    for i in sys.argv[1:]:
        args = args+i+";"
    for arg in arg_bonus:
        args = args + arg[1] + ";"
    return args


# Paramétrage des arguments
# Création de l'ensemble d'utilisateur·ices
try:
    csvfile = sys.argv[15]
    user_set = csvimport.import_csv(csvfile, n_user)
except IndexError:
    used_users = users.choose(user_type)
    user_set = used_users(n_user, dimension)


# Création du nœud initial
# eps_init = 100
eps_init = eps.quad_count(epsilon, 0, prof_max, ratio_count)
node = t.Node([[0,1]]*dimension, n_user, user_set, eps_init, (eps_init**2)*n_user)


def kd_print (mini):
    tree = kdtree.kdtree(user_set, prof_max, bins, epsilon, node, prof_max, eps.quad_count, eps.uniform_median, dim.next_unused, used_median, used_count, ratio_count, 2, coalition)
    consist_tree = consistency.compute(tree, prof_max, epsilon, "quad", eps_init, 2, ratio_count)
    if mini:
        mini_results = arguments_recus()
    else:
        mini_results = ""
        if printer:
            consist_tree.printTree()
        for i in range(len(sys.argv)-1):
            print(arguments[i]+" : "+sys.argv[i+1])
        for arg in arg_bonus:
            print(arg[0]+" : "+arg[1])
    return [consist_tree,mini_results]


cons = kd_print(mini)

# tasks = v_task.choose(format_tache)(nb_tasks, dimension, 1)

eval_queries = evaluation.eval_global(format_tache, nb_tasks, user_set, dimension, cons[0])


if not mini:
    print("\nErreur relative moyenne : "+str(eval_queries[0]))
    print("Erreur moyenne (quadratique) : "+str(eval_queries[1]))
    print("Erreur moyenne (linéaire) : "+str(eval_queries[2]))
    print("Ecart type de l'erreur : "+str(eval_queries[3]))
    print("Max tasks for a leaf : "+str(eval_queries[4]))
    print("Tasks with no users (unpredicted) : "+str(eval_queries[5]))

# On a donc : moyenne quadratique et linéaire, et écart-type de l'erreur (l'erreur vaut en moyenne ça, mais des fois, c'est vachement plus élevé/c'est toujours très proche de ça)
else:
    cons[1] = cons[1]+str(eval_queries[0])+";"+str(eval_queries[1])+";"+str(eval_queries[2])+";"+str(eval_queries[3])+";"+str(eval_queries[4])+";"+str(eval_queries[5])
    print(cons[1])
