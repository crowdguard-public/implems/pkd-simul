import numpy
from scipy.stats import nbinom
from operator import itemgetter

def is_in(user, volume, dim):
    return all(
        (volume[d][0] <= user[d] < volume[d][1] or
         (volume[d][1] == 1 and
          volume[d][0] <= user[d] <= volume[d][1]
          )
         )
        for d in range(dim)
    )


def data_indep(epsilon, div_dim, volume, matrix_user, bins, fanout, coalition):
    n_user = len(matrix_user)
    dim = len(volume)
    number_in = sum(is_in(u, volume, dim) for u in matrix_user)
    mini = volume[div_dim][0]
    maxi = volume[div_dim][1]
    ecart = maxi - mini
    divisions = []
    for i in range(fanout)+1:
        divisions = mini + (i*ecart)/fanout
    return [divisions,number_in/fanout]


def precise_med(epsilon, div_dim, volume, matrix_user, bins,fanout, coalition):
    copy = matrix_user[:]
    dim = len(volume)
    copy = [user for user in copy if is_in(user, volume, dim)]
    sorted_dim = sorted(copy, key=itemgetter(div_dim))
    divisions = [volume[div_dim][0]]
    people_in = len(sorted_dim)
    #patch bizarre pour le cas où il n'y a personne dedans (la médiane précédente vaut la borne de la zone par exemple, avec la distribution "zéro" d'utilisateurs)
    if people_in == 0:
        return [[volume[div_dim][0]]*(fanout+1), 0]
    for i in range(fanout-1):
        divisions = divisions + [sorted_dim[int((i+1)*people_in/fanout)][div_dim]]
    divisions = divisions +[volume[div_dim][1]]
    return [divisions, people_in/fanout]


def hist(epsilon, div_dim, volume, matrix_user, bins,fanout, coalition):
    n_user = len(matrix_user)
    dim = len(volume)
    hist = []
    part = []
    debut = volume[div_dim][0]
    fin = volume[div_dim][1]
    taille_part = (fin-debut)/bins
    for i in range(bins+1):
        part = part + [debut + i*taille_part]

    #on fait des counts de chaque sous-zone
    for b in range(bins):
        hist = hist + [sum((is_in(user, volume, dim) & (part[b] <= user[div_dim] < part[b+1])) for user in matrix_user)]
    somme = sum(hist)
    count_before = 0
    bins_before = 0
    median = []
    for i in range(fanout-1):
        #On ne peut pas être à bins-1 (parce que "before" et bins-1 est le dernier bin)
        while (bins_before < bins-2) :
            temporaire = count_before + hist[bins_before]
            if temporaire >= (i+1)*somme/fanout:
                break
            bins_before = bins_before +1
            count_before = temporaire
        count_middle = hist[bins_before]
        if count_middle == 0:
            #Convention… mathématiquement, c'est équivalent…
            median = median + [debut + taille_part*(bins_before + ((i+1)/fanout))]
        else:
            median = median + [debut + taille_part*(bins_before + (1/(fanout*count_middle))*(somme*(i+1)-fanout*count_before))]
    return [[debut]+median+[fin], somme/fanout]


def noisy_hist(epsilon, div_dim, volume, matrix_user, bins,fanout, coalition):
    n_user = len(matrix_user)
    dim = len(volume)
    hist = []
    part = []
    debut = volume[div_dim][0]
    fin = volume[div_dim][1]
    taille_part = (fin-debut)/bins
    for i in range(bins+1):
        part = part + [debut + i*taille_part]

    #on fait des counts de chaque sous-zone
    for b in range(bins):
        hist = hist + [sum((is_in(user, volume, dim) & (part[b] <= user[div_dim] < part[b+1])) for user in matrix_user)]

    #on rajoute le bruit
    #TODO : BIEN REVOIR "p" !!!
    p = 1-numpy.exp(-epsilon)
    for b in range(bins):
        #génération du bruit
        list_noise_plus = nbinom.rvs(1/(n_user-coalition), p, 0, n_user)
        geom_plus = sum(list_noise_plus)
        list_noise_moins = nbinom.rvs(1/(n_user-coalition), p, 0, n_user)
        geom_moins = sum(list_noise_moins)
        noise = geom_plus - geom_moins
        #Que se passe-t-il si un bin est < 0 ? Le plus probable est : ça vaut 0. Et mettre à 0 simplifie la médiane.
        hist[b] = max(hist[b]+noise,0)
    #on prend la médiane de l'histogramme
    somme = sum(hist)
    count_before = 0
    bins_before = 0
    median = []
    for i in range(fanout-1):
        #On ne peut pas être à bins-1 (parce que "before" = bins-2 et bins-1 est le dernier bin)
        while (bins_before < bins-2) :
            temporaire = count_before + hist[bins_before]
            if temporaire >= (i+1)*somme/fanout:
                break
            bins_before = bins_before +1
            count_before = temporaire
        count_middle = hist[bins_before]
        if count_middle == 0:
            #Convention… mathématiquement, c'est équivalent…
            median = median + [debut + taille_part*(bins_before + ((i+1)/fanout))]
        else:
            median = median + [debut + taille_part*(bins_before + (1/(fanout*count_middle))*(somme*(i+1)-fanout*count_before))]
    return [[debut]+median+[fin], somme/fanout]

def choose(entree):
    if entree == "noisy_hist":
        return noisy_hist
    elif entree == "hist":
        return hist
    elif entree == "data_indep":
        return data_indep
    elif entree == "precise":
        return precise_med
    else :
        print("Stratégie de médiane inconnue. Les stratégies possibles sont \"noisy_hist\", \"precise\", \"data_indep\" ou \"hist\"")
