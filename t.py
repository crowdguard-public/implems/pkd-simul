import math
import numpy


class Node:
    def __init__(self, volume, app_count, people, epsilon, alpha):
        self.volume = volume
        self.app_count = app_count
        self.people = people
        self.epsilon = epsilon
        self.real_count = len(people)
        self.dim = len(volume)
        self.z = -1
        self.f = -1
        self.beta = -1
        self.alpha = alpha
        self.center = list(map(numpy.mean, self.volume))

    def getCenter(self):
        return self.center

    def getRealcount(self):
        return self.real_count

    def getCount(self):
        return self.app_count

    def getZ(self):
        return self.z

    def getF(self):
        return self.f

    def getAlpha(self):
        return self.alpha

    def getBeta(self):
        return self.beta

    def getEpsilon(self):
        return self.epsilon

    def getVolume(self):
        return self.volume

    def getDim(self):
        return self.dim

    def getPeople(self):
        return self.people

    def delUser(self, index):
        del self.people[index]
        self.real_count = self.real_count -1
        self.beta = self.beta -1

    def setZ(self, new_z):
        self.z = new_z

    def setF(self, new_f):
        self.f = new_f

    def setBeta(self, new_beta):
        self.beta = new_beta

    def isIn(self, task):
        return all(self.volume[d][0] <= task[d] < self.volume[d][1] for d in range(self.dim))

    def getDist(self, task, dist):
        return dist(self.center, task)

    def calcVolume(self):
        v = 1
        for i in range(self.dim):
            v = v*(self.volume[i][1] - self.volume[i][0])
        return v

    def print_line(self,string, cut, table_cut):
        to_print = ""
        l = len(string)
        for i in range(l):
            if i == l-1:
                if string[i] == "l":
                    to_print = to_print+"├--"
                elif string[i] == "r":
                    to_print = to_print+"└--"
            else:
                if string[i] == "l":
                    to_print = to_print + "|  "
                elif string[i] == "r":
                    to_print = to_print + "   "
        if self.epsilon == -1:
            print(to_print+"("+str(self.real_count)+", -1)")
        else:
            puissance_eps = math.floor(math.log(self.epsilon, 10))
            if cut == -1:
                print(to_print+"(app:"+str(int(self.app_count))+" cons:"+str(round(self.beta,2))+" reel:"+str(self.real_count)+" eps:" +str(round((self.epsilon*(10**-puissance_eps)),2)) + "e" + str(puissance_eps)+")")
            else:
                print(to_print+"(app:"+str(int(self.app_count))+" cons:"+str(round(self.beta,2))+" reel:"+str(self.real_count)+" eps:" +str(round((self.epsilon*(10**-puissance_eps)),2)) + "e" + str(puissance_eps)+", cut:"+str(cut)+" "+str(list(int(x*1000)/1000 for x in table_cut))+")")


class Tree:
    def __init__(self, node, children, cut, table_cut):
        self.cut = cut
        self.table_cut = table_cut
        self.children = children
        self.node = node
        self.null = (len(children) == 0)
        self.fanout = len(children)

    def getTablecut(self):
        return self.table_cut

    def getCut(self):
        return self.cut

    def getFanout(self):
        return self.fanout

    def getChildren(self):
        return self.children

    def getChild(self,numero):
        return self.children[numero]

    def getNode(self):
        return self.node

    def isNull(self):
        return self.null

    def printTree_rec(self, string):
        if self.null:
            self.node.print_line(string, self.cut, self.table_cut)
        else:
            self.node.print_line(string, self.cut, self.table_cut)
            for i in range(self.fanout):
                if i == (self.fanout - 1):
                    self.children[i].printTree_rec(string+"r")
                else:
                    self.children[i].printTree_rec(string+"l")

    def printTree(self):
        print("(app:"+str(int(self.node.app_count))+"; cons:"+str(round(self.node.beta,2))+"; reel:"+str(self.node.real_count)+"; eps:" +str(self.node.epsilon)+", cut:"+str(self.cut)+" "+str(list(int(x*1000)/1000 for x in self.table_cut))+")")
        for i in range(self.fanout):
            if i == (self.fanout -1):
                self.children[i].printTree_rec("r")
            else:
                self.children[i].printTree_rec("l")

    def getLeaves(self):
        if self.isNull():
            return [self.node]
        else:
            leaves = []
            for ch in self.children:
                leaves = leaves + ch.getLeaves()
            return leaves

    def get_leaves_at_depth(self, depth=0):
        if depth == 0 or self.isNull():
            return [self.node]
        else:
            leaves = []
            for ch in self.children:
                leaves = leaves + ch.get_leaves_at_depth(depth - 1)
            return leaves
