import random


def zero(volume, prof_act, prof_max):
    return 0


def random_dim (volume, prof_act, prof_max):
    return random.randint(0, len(volume)-1)


def next_unused(volume, prof_act, prof_max):
    return (prof_max-prof_act) % len(volume)


def choose(entree):
    if entree == "zero":
        return zero
    elif entree == "random_dim":
        return random_dim
    elif entree == "next_unused":
        return next_unused
    else:
        print("stratégie de choix de dimension inconnue. Les stratégies possibles sont \"zero\", \"random_dim\" et \"next_unused\"")
