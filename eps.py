def constante(eps, h):
    return eps*(2**(1/3)-1)/(2**((h+1)/3)-1)


def uniform_median(epsilon, prof_act, prof_max, ratio_count):
    e = epsilon*(1-ratio_count)
    return e / (prof_max)


def uniform_count(epsilon, prof_act, prof_max, ratio_count):
    e = epsilon*ratio_count
    return e / prof_max


def quad_median(epsilon, prof_act, prof_max, ratio_count):
    e = epsilon*(1-ratio_count)
    #Le "prof_max+1" vient du fait qu'on ne compte pas la racine. En vrai, on devrait faire "prof_max -1 à chaque fois, et "prof_act+1" à chaque fois aussi.
    res = 2**((prof_max - prof_act)/3)*constante(e, prof_max-1)
    return res


def quad_count(epsilon, prof_act, prof_max, ratio_count):
    e = epsilon*ratio_count
    c = constante(e, prof_max-1)
    res = 2**((prof_max - prof_act)/3)*c
    return res


def choose_count(entree):
    if entree == "quad":
        return quad_count
    elif entree == "unif":
        return uniform_count
    else:
        print("stratégie de distribution des epsilons inconnue. Les stratégies possibles sont : \"quad\" et \"unif\"")


def choose_median(entree):
    if entree == "quad":
        return quad_median
    elif entree == "unif":
        return uniform_median
    else:
        print("stratégie de distribution des epsilons inconnue. Les stratégies possibles sont : \"quad\" et \"unif\"")





"""
However, according to [7], the latter is more efficient, and we can hence assume it as a baseline strategy. This would give, for a tree of size h, a budget  and for a level i (starting from 0 for the leaves, to h for the root), the partial budget :

    e_i = 2^{(h-i)/3} \epsilon \frac{racine_troisième_de_2 - 1}{2^{(h+1)/3}-1}
    """




