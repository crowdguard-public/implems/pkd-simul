import random
from scipy.stats import uniform


def tasks_unif(dim, k, tree):
    """
    Generate a single uniform task
    :param dim:
    :param k:
    :param tree:
    :return:
    """
    task = []
    for d in range(dim):
        task += [sorted(uniform.rvs(0, 1, 2))]
    task = (task, random.randint(1, k))
    return task


def random_tasks_one_spe(dim, k, tree):
    """
    Generate a task requiring a worker with one single speciality
    :param dim:
    :param k:
    :param tree:
    :return:
    """
    spe = random.randint(0, dim - 1)
    task = []
    for d in range(dim):
        if d == spe:
            task += [[uniform.rvs(0.5, 0.5, 1)[0], 1]]
        else:
            task += [[0, uniform.rvs(0, 0.5, 1)[0]]]
    task = (task, random.randint(1, k))
    return task


def random_tree_nodes(tree, k, depth):
    """
    Get some random nodes from a tree depth
    """
    nodes = tree.get_leaves_at_depth(depth)
    tree = random.choice(nodes)
    task = (tree.volume, random.randint(1, k))
    return task


def generate_tasks(args, tree):
    """
    Generate tasks from the command line arguments
    :param args: CLI arguments
    :param tree: PKD tree
    :return:
    """
    dims = args.dims
    k = 1

    if args.tasks_unif:
        return tasks_unif(dims, k, tree)
    if args.tasks_onespe:
        return random_tasks_one_spe(dims, k, tree)
    if args.tasks_tree is not None:
        depth = args.tasks_tree
        return random_tree_nodes(tree, k, depth)

    print(args)
    print("Invalid tasks creation")
    exit(-1)


def choose(entree):
    if entree == "random":
        # ok pour nbre de tâches max
        return tasks_unif
    elif entree == "one_spe":
        # optimal par rapport au nombre de tâches max quand dim ~= hauteur de l'arbre
        return random_tasks_one_spe
    else:
        print('type de format de tâches inconnu. Les formats autorisés sont : "random", "pareto", "one_spe", '
              '"random_square", "random_square_3cases" et "random_square_corner"')
