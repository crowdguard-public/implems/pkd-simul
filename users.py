import random
import csv
from scipy.stats import uniform,truncnorm

import csvimport


def set_unif(n_users, dim):
    new_set = []
    for u in range(n_users):
        new_set += [uniform.rvs(0, 1, dim)]
    return new_set

"""
def set_one_spe(n_users, dim):
    new_set = []
    for u in range(n_users):
        k = random.randint(0, dim-1)
        new_set += [uniform.rvs(0, 0.5, dim)]
        new_set[-1][k] = uniform.rvs(0.5, 0.5, 1)[0]
    return new_set
"""

def set_one_spe(n_users, dim):
    new_set = []
    for u in range(n_users):
        k = random.randint(0, dim-1)
        w = [0] * dim
        w[k] = 1
        new_set += [w]
    return new_set


def generate_users(args):
    """
    Generate workers from command line arguments
    :param args: CLI Arguments
    :return:
    """
    nusers = args.nusers
    dim = args.dims

    if args.users_unif:
        return set_unif(nusers, dim)
    elif args.users_onespe:
        return set_one_spe(nusers, dim)
    elif args.users_csv:
        csv = args.users_csv
        return csvimport.import_csv(csv, nusers)
    else:
        print("Invalid user type")
        exit(-1)


def dump_users(args, users):
    """
    Dump the user set in a CSV file of the name of the experiment.
    :param args:
    :param users:
    :return:
    """
    if args.wdump:
        with open(args.wdump, 'w') as csvfile:
            for user in users:
                writer = csv.writer(csvfile)
                writer.writerow(user)


def choose(entree):
    if entree == "unif":
        return set_unif
    elif entree == "one_spe":
        return set_one_spe
    else:
        print("type d'utilisateur·ice inconnu. Les types autorisés sont : \"unif\", \"one_spe\"")
