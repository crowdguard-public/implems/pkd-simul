#!/bin/bash

if [ $# -eq 0 ]
then
	MAIN_PATH='main.py'
	KDEXPE='./expe'
	IGRIDA=0
else
	MAIN_PATH='/udd/'$USER'/Documents/crowdguard/ppcrowd/kdtrees/prog/python/main_ter.py'
	KDEXPE='/temp_dd/igrida-fs1/'$USER'/SCRATCH/expes_kdtrees'
	IGRIDA=1
fi

########## Global vars ##########
DATE=`date +%Y-%m-%d-%H-%M-%S`
# Number of reproductions
REPRODUCTIONS=1
# Number of cores
NCORES=4
# Number of users
NUSERS=10000
# Number of tasks
NTASKS=1000
# Workers CSV file
CSV=./ratiouv_10.csv
# Number of dimensions
DIMS=10

# Count strategy (noisy, precise)
STRAT_COUNT=--sc_noisy
# Median strategy (noisy_hist, hist)
STRAT_MED=--sm_noisy
# Expe archive name
ARCHNAME=$DATE-$REPRODUCTIONS-$DIMS-$NUSERS-$NTASKS
# Expe archive path
KDEXPE=$KDEXPE/$ARCHNAME
# Expe workers CSV dump path
EXPEWORKERS=$KDEXPE/workers

########### DEPTH ##########
DEPTH_OUT=$KDEXPE/depth.csv
DEPTH_VAR=(4) # 6 8 10 12
DEPTH_BINS=10
DEPTH_EPSILON=0.1

function cmd_depth {
	# $1 : variable parameter
	# $2 : workers generation mode
	# $3 : task generation mode
	echo "python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $1 --bins $DEPTH_BINS --epsilon $DEPTH_EPSILON --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED >> $DEPTH_OUT"

    WORKERS=($2)
	DUMP="$EXPEWORKERS/depth-$DIMS-$NUSERS-$1-$DEPTH_BINS-$DEPTH_EPSILON${WORKERS[0]}-$NTASKS.csv"
	
	python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $1 --bins $DEPTH_BINS --epsilon $DEPTH_EPSILON --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED --wdump $DUMP >> $DEPTH_OUT
}

########## BINS ##########
BINS_OUT=$KDEXPE/bins.csv
BINS_VAR=(1 5 10 15 20)
BINS_DEPTH=10
BINS_EPSILON=0.1

function cmd_bins {
	# $1 : variable parameter
	# $2 : workers generation mode
	# $3 : task generation mode
	echo "python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $BINS_DEPTH --bins $1 --epsilon $BINS_EPSILON --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED >> $BINS_OUT"

	WORKERS=($2)
	DUMP="$EXPEWORKERS/depth-$DIMS-$NUSERS-$BINS_DEPTH-$1-$BINS_EPSILON${WORKERS[0]}-$NTASKS.csv"
	
	python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $BINS_DEPTH --bins $1 --epsilon $BINS_EPSILON --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED --wdump $DUMP >> $BINS_OUT
}

########## EPSILON ##########
EPSILON_OUT=$KDEXPE/epsilon.csv
EPSILON_VAR=(0.01 0.1 1 10)
EPSILON_DEPTH=10
EPSILON_BINS=10

function cmd_epsilon {
	# $1 : variable parameter
	# $2 : workers generation mode
	# $3 : task generation mode
	echo "python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $EPSILON_DEPTH --bins $EPSILON_BINS --epsilon $1 --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED >> $EPSILON_OUT"

	WORKERS=($2)
	DUMP="$EXPEWORKERS/depth-$DIMS-$NUSERS-$EPSILON_DEPTH-$EPSILON_BINS-$1${WORKERS[0]}-$NTASKS.csv"
	
	python3 $MAIN_PATH --short --dims $DIMS --nusers $NUSERS --depth $EPSILON_DEPTH --bins $EPSILON_BINS --epsilon $1 --eps 0.7 $2 --coalition 1 --ntasks $NTASKS $3 $STRAT_COUNT $STRAT_MED --wdump $DUMP >> $EPSILON_OUT
}

########## MISC FUNCTIONS #########
function expe_loop {
	# $1 : experiment command function
	# $2 : name of the variable containing the array of the variable parameter
	# $3 : name of the workers generation method
	# $4 : name of the tasks generation method
	CMD=$1
	VARS=$2[@]
	VARS=( "${!VARS}" )
	GEN_WORKERS=$3
	GEN_TASKS=$4


	for i in ${VARS[@]}; do
		NB=0

		while [ $NB -lt $REPRODUCTIONS ]; do
			for j in $(seq 1 $NCORES); do
				$CMD $i "$GEN_WORKERS" "$GEN_TASKS" &
				((NB++))
				if [ $NB -ge $REPRODUCTIONS ]; then
					break
				fi
			done
			wait
		done
	done
}

function expe {
	# $1 : experiment command function
	# $2 : name of the variable containing the array of the variable parameter
	echo "unif"
	#expe_loop $1 $2 --users_unif --tasks_unif
	echo "one_spe"
	expe_loop $1 $2 --users_onespe "--tasks_tree 0"
	echo "csv"
	expe_loop $1 $2 "--users_csv $CSV" "--tasks_tree 0"
}


function write_params {
	FILE=$KDEXPE/params.txt

	echo "Name : $ARCHNAME" >> $FILE
	echo ""

	echo "Date : $DATE" >> $FILE
	echo "Reproductions : $REPRODUCTIONS" >> $FILE
	echo "Number of users : $NUSERS" >> $FILE
	echo "Number of tasks : $NTASKS" >> $FILE
	echo "Count strategy : $STRAT_COUNT" >> $FILE
	echo "Median strategy : $STRAT_MED" >> $FILE
	echo "CSV file : $CSV" >> $FILE

	echo "" >> $FILE
	echo "expe DEPTH : " >> $FILE
	echo "  depth variation : ${DEPTH_VAR[@]}" >> $FILE
	echo "  bins : $DEPTH_BINS" >> $FILE
	echo "  epsilon : $DEPTH_EPSILON" >> $FILE

	echo "" >> $FILE
	echo "  depth : $BINS_DEPTH" >> $FILE
	echo "  bins variation : ${BINS_VAR[@]}" >> $FILE
	echo "  epsilon : $BINS_EPSILON" >> $FILE

	echo "" >> $FILE
	echo "expe EPSILON : " >> $FILE
	echo "  depth : $EPSILON_DEPTH" >> $FILE
	echo "  bins : $EPSILON_BINS" >> $FILE
	echo "  epsilon variation : ${EPSILON_VAR[@]}" >> $FILE

  cp ./full_expe.sh $KDEXPE/full_expe.sh
}

########## MAIN ###########

echo Name : $ARCHNAME

# Check if output directories exist
if [ ! -d $KDEXPE ]; then
	mkdir -p $KDEXPE
fi
if [ ! -d $EXPEWORKERS ]; then
	mkdir -p $EXPEWORKERS
fi


echo "start experiment"

write_params

echo "DEPTH experiments"
expe cmd_depth DEPTH_VAR

echo "BINS experiments"
#expe cmd_bins BINS_VAR

echo "EPSILON experiments"
#expe cmd_epsilon EPSILON_VAR

echo "end experiment"

echo "graph"
python3 ./graph.py $KDEXPE

EXPE_NOZIP=tt

if [ -z ${EXPE_NOZIP} ]; then
    echo "zip"
    zip -r $KDEXPE.zip $KDEXPE
fi

echo ""
echo Name : $ARCHNAME
