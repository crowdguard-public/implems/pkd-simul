import t
from scipy.stats import uniform
from scipy.spatial import distance
import statistics
import v_task


def is_in(user, volume, dim):
    return all(
        (volume[d][0] <= user[d] < volume[d][1] or
         (volume[d][1] == 1 and
          volume[d][0] <= user[d] <= volume[d][1]
          )
         )
        for d in range(dim)
    )


def real_count(users, volume, dim):
    return sum(is_in(user, volume, dim) for user in users)


def inclusion_value(task, leaf, dim):
    res = 1
    for d in range(dim):
        if (task[d][1] <= leaf[d][0]) or (task[d][0] >= leaf[d][1]):
            return 0
        else:
            if leaf[d][1] - leaf[d][0] == 0:
                inclusion_dim = 1
            else:
                ecart_inclus = min([task[d][1] - task[d][0], leaf[d][1]-task[d][0], task[d][1]-leaf[d][0], leaf[d][1]-leaf[d][0]])
                inclusion_dim = ecart_inclus/(leaf[d][1]-leaf[d][0])
            res = res*inclusion_dim
    return res


def max_tasks(tasks, leaves, dim):
    l_nbre_tasks = []
    for l in leaves:
        v_leaf = l.getVolume()
        nbre_t = 0
        for t in tasks:
            if inclusion_value(v_leaf, t[0], dim) > 0:
                nbre_t += 1
        l_nbre_tasks += [nbre_t]
    return max(l_nbre_tasks)


def max_user_leaf(leaves):
    liste_users = []
    for l in leaves:
        liste_users = liste_users + [l.getRealcount()]
    return max(liste_users)


def approx_count_volume(vol, leaves, dim):
    result = 0
    for l in leaves:
        c = round(l.getBeta())
        v_leaf = l.getVolume()
        result = result + inclusion_value(vol, v_leaf, dim)*c
    return result


def eval_global(args, nb_tasks, users, dim, tree):
    leaves = tree.getLeaves()

    list_res = []
    l_exact = []
    l_approx = []
    relative = []
    exact_null = 0
    non_empty_tasks = 0
    selected_tasks = []

    while non_empty_tasks < nb_tasks:
        task = v_task.generate_tasks(args, tree)

        exact = real_count(users, task[0], dim)
        if exact != 0:
            approx = approx_count_volume(task[0], leaves, dim)
            l_exact += [exact]
            l_approx += [approx]
            list_res += [abs(approx - exact)]
            relative += [abs(1 - approx / exact)]
            non_empty_tasks += 1
            selected_tasks += [task]
        else:
            print("empty : " + str(non_empty_tasks))

    if nb_tasks - exact_null != 0:
        relative_res = statistics.mean(relative)
        # relative_res + i/(nb_tasks - exact_null)
        return [relative_res,
                distance.euclidean(l_exact, l_approx) / (nb_tasks**(1/2)),
                distance.cityblock(l_exact, l_approx) / nb_tasks,
                statistics.pstdev(list_res),
                max_tasks(selected_tasks, leaves, dim),
                exact_null
                ]
    else:
        return ["toutes les tâches sont vides",
                distance.euclidean(l_exact, l_approx) / (nb_tasks**(1/2)),
                distance.cityblock(l_exact, l_approx) / nb_tasks,
                statistics.pstdev(list_res),
                max_tasks(selected_tasks, leaves, dim),
                exact_null
                ]
