import t
import evaluation
from operator import itemgetter

# On considère l'ordre sur les tâches et l'ordre sur les feuilles fixés.
# L'assignation d'une tâche est la liste des indices des feuilles qui lui sont assignées.
# Une assignation globale est la liste de ces assignations de tâches (dans l'ordre des tâches)


# calcule une valeur approché du nombre de personnes ok dans une zone
def approx_use_leave(task, leave, dim):
    number_required = task[1]
    people_in_leave = leave.getBeta()
    #pourcentage d'occupation de la feuille par la tâche
    collusion = evaluation.inclusion_value(task[0], leave.getVolume(), dim)
    potential = collusion*people_in_leave
    usefull = min(number_required, potential)
    useless = people_in_leave - usefull
    return[usefull, useless, potential]


def approx_use_leave_for_sort(task, leave, dim):
    [usefull, useless, potential] = approx_use_leave(task, leave, dim)
    return [-usefull, useless, -potential]


def unique_heur(sort, leaves, task, dim):
    new_list = sort(leaves, task, dim)
    return [new_list[0][0]]


def greedy_heur(sort, leaves, task, dim):
    new_list = sort(leaves, task, dim)
    done = 0
    required = task[1]
    assigned = []
    while (required > done) & (new_list != []):
        assigned = assigned + [new_list[0][0]]
        done = done + approx_use_leave(task, leaves[new_list[0][0]], dim)[0]
        del(new_list[0])
    return assigned


def soustraire_new_useless(zone, required, done):
    zone[2] = zone[2] + max(0, done+zone[1]-required)
    zone[1] = min(zone[1], required-done)
    return zone


def resort(liste, required, done):
    opt = liste[0]
    done_bis = done + opt[0]
    del(liste[0])
    liste_bis = list(map(lambda zone : soustraire_new_useless(zone, required, done_bis), liste))
    liste_bis = sorted(liste_bis, key = itemgetter(1))
    return (opt, done_bis, liste_bis)


def greedy_heur_resort(sort, sort_bis, leaves, task, dim):
    new_list = sort(leaves, task, dim)
    done = 0
    required = task[1]
    assigned = []
    while (required > done) & (new_list != []):
        assigned = assigned + [new_list[0][0]]
        del(new_list[0])
        #Suppose que le résultat du tri soit sous la forme [?, utile, inutile, ???]
        new_list = list(map(lambda zone : soustraire_new_useless(zone, required, done), new_list))
        new_list = sort_bis(new_list)
    return assigned


def best_portion_sort(leaves, task, dim):
    non_tri = list(map(lambda leave : evaluation.inclusion_value(leave.getVolume(), task[0], dim), leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + [non_tri[i]]
    return sorted(non_tri, key = itemgetter(1))


def min_useless_max_usefull_sort(leaves, task, dim):
    non_tri = list(map(lambda leave : approx_use_leave_for_sort(task, leave, dim), leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + non_tri[i]
    return sorted(non_tri, key = itemgetter(2,1,3))


def max_usefull_min_useless_sort(leaves, task, dim):
    non_tri = list(map(lambda leave : approx_use_leave_for_sort(task, leave, dim), leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + non_tri[i]
    return sorted(non_tri, key = itemgetter(1,2,3))


def max_potential_min_useless_sort(leaves, task, dim):
    non_tri = list(map(lambda leave : approx_use_leave_for_sort(task, leave, dim), leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + non_tri[i]
    return sorted(non_tri, key = itemgetter(3,1,2))


def min_useless_max_potential_sort(leaves, task, dim):
    non_tri = list(map(lambda leave : approx_use_leave_for_sort(task, leave, dim), leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + non_tri[i]
    return sorted(non_tri, key = itemgetter(1,3,2))


def usefull_proportion_sort (leaves, task, dim):
    non_tri = list(map(lambda leave : [approx_use_leave(task, leave, dim)[0]/(approx_use_leave(task, leave, dim)[1]+1)], leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + [non_tri[i]]
    return sorted(non_tri, key = itemgetter(1))


def potential_proportion_sort (leaves, task, dim):
    non_tri = list(map(lambda leave : [approx_use_leave(task, leave, dim)[2]/(approx_use_leave(task, leave, dim)[1]+1)], leaves))
    for i in range(len(non_tri)):
        non_tri[i] = [i] + [non_tri[i]]
    return sorted(non_tri, key = itemgetter(1))





"""

#On prend la feuille qui contient la plus grande partie de la tâche
def portion_task_in_leave(leave, task, dim):
    return evaluation.inclusion_value(leave.getVolume(), task[0], dim)

#naif : on prend la zone qui contient la plus grande portion de la tache
def unique_naive (task, leaves, dim):
#Quel pourcentage de v_task est dans leave (on imagine que v2 explorera les feuilles de l'arbre)
    liste_inclusion = list(map(lambda leave : portion_task_in_leave(leave,task,dim), leaves))
    return [liste_inclusion.index(max(liste_inclusion))]


#comme naif, mais on les prends toutes jusqu'à en avoir assez
def unique_naive_multi (task, leaves, dim):
    assigned = []
    liste_inclusion = list(map(lambda leave : [portion_task_in_leave(leave,task,dim)], leaves))
    for i in range(len(liste_inclusion)):
        liste_inclusion[i] = liste_inclusion[i]+[i]
    sorted_liste = sorted(liste_inclusion, key = itemgetter(0))
    required = task[1]
    done = 0
    while (required > done) & (sorted_liste != []):
        done = done + approx_use_leave(task, leaves[sorted_liste[0][1]], dim)[0]
        assigned = assigned + [sorted_liste[0][1]]
        del(sorted_liste[0])
    return assigned


#on trie les zones suivant le nombre d'inutiles contactés, on prend dans l'ordre jusqu'à contacter assez de monde
def unique_greedy_naive (task, leaves, dim):
    required = task[1]
    done = 0
    assigned = []
    liste_use = list(map(lambda leave : lexico_use_leave(task, leave, dim), leaves))
    #pour garder les indices initiaux malgré le tri
    for i in range(len(liste_use)):
        liste_use[i] = liste_use[i]+[i]
    sorted_liste = sorted(liste_use, key = itemgetter(1,0))
    while (required > done) & (sorted_liste != []):
        assigned = assigned + [sorted_liste[0][2]]
        done = done + sorted_liste[0][0]
        del(sorted_liste[0])
    return assigned



#cas d'usage :
#une tâche qui demande 4
#zone 1 : 3, 0
#zone 2 : 3, 0
#zone 3 : 1, 1
#optimal : zones 1 et 3 (1 inutile)
#greedy : zones 1 et 2 (2 inutiles)
#conclusion : il faut tenir compte de ce qui a déjà été pris
#pour re-trier : pour chaque zone, on diminue le usefull à au plus required-done, et on augmente le useless de (ancien_usefull + done - required) puis on retrie suivant le useless


def resort(liste, required, done):
    opt = liste[0]
    done_bis = done + opt[0]
    del(liste[0])
    liste_bis = list(map(lambda zone : soustraire_new_useless(zone, required, done_bis), liste))
    liste_bis = sorted(liste_bis, key = itemgetter(1))
    return (opt, done_bis, liste_bis)


def unique_greedy_resort(task, leaves, dim):
    required = task[1]
    liste_use = list(map(lambda leave : lexico_use_leave(task, leave, dim), leaves))
    indice = 0
    #pour garder les indices initiaux malgré le tri
    for i in range(len(liste_use)):
        liste_use[i] = liste_use[i]+[i]
    sorted_liste = sorted(liste_use, key = itemgetter(1,0))
    assigned = [sorted_liste[0][2]]
    done = sorted_liste[0][0]
    while (required > done) & (sorted_liste != []):
        (opt, done, sorted_liste) = resort(sorted_liste, required, done)
        done = done + opt[0]
        assigned = assigned + [opt[2]]
    return assigned


def unique_greedy_proportion (task, leaves, dim):
    required = task[1]
    done = 0
    assigned = []
    liste_use = list(map(lambda leave : [approx_use_leave(task, leave, dim)[0]/(approx_use_leave(task, leave, dim)[1]+1)], leaves))
    #pour garder les indices initiaux malgré le tri
    for i in range(len(liste_use)):
        liste_use[i] = liste_use[i]+[i]
    sorted_liste = sorted(liste_use, key = itemgetter(0), reverse=True)
#    while (liste_use[0][0]> 0.1) & (sorted_liste != []) & (required > done):
    while (sorted_liste != []) & (required > done):
        assigned = assigned + [sorted_liste[0][1]]
        done = done + approx_use_leave(task, leaves[sorted_liste[0][1]], dim)[0]
        del(sorted_liste[0])
    return assigned


"""


def best(tasks, leaves, dim):
    result = 0
    for t in tasks:
        result_t = 0
        for l in leaves:
            result_t = result_t + evaluation.real_count(l.getPeople(), t[0], dim)
        result = result + min(result_t, t[1])
    return result


def multi_method(method, leaves, tasks, dim):
    return list(map(lambda task : method(leaves, task, dim), tasks))


def use_leave(task, leave, required, dim):
    people_in_leave = leave.getPeople()
    good_people = evaluation.real_count(people_in_leave, task[0], dim)
    usefull = min(required, good_people)
    useless = len(people_in_leave) - usefull
    return[usefull, useless]


def quality_assignment(leaves, tasks, dim, assignation):
    assignment = multi_method(assignation, leaves, tasks, dim)
    overcontact = 0
    undercontact = 0
    success = 0
    total = sum(leave.getRealcount() for leave in leaves)*len(tasks)
    for t in range(len(tasks)):
        number_required = tasks[t][1]
        for l in assignment[t]:
            [usefull, useless] = use_leave(tasks[t], leaves[l], number_required, dim)
            success = success + usefull
            number_required = number_required - usefull
            overcontact = overcontact + useless
        undercontact = undercontact + number_required
    recall = success/best(tasks, leaves, dim)
    if (overcontact + success == 0):
        precision = 0
    else:
        precision = success/(overcontact + success)
    if (precision + recall == 0):
        f = 0
    else:
        f = 2*precision*recall/(precision + recall)
    return (recall, precision, f)


def choose(entree):
    if entree == "naive":
        return lambda leaves, tasks, dim: quality_assignment(leaves, tasks, dim, unique_naive)
    elif entree == "greedy_naive":
        return lambda leaves, tasks, dim: quality_assignment(leaves, tasks, dim, unique_greedy_naive)
    elif entree == "greedy_resort":
        return lambda leaves, tasks, dim: quality_assignment(leaves, tasks, dim, unique_greedy_resort)
    else:
        print("stratégie d'assignation des zones inconnue. Les stratégies possibles sont \"naive\", \"greedy_naive\" et \"greedy_resort\".")


def evaluate(leaves, tasks, dim):
    sorts = [best_portion_sort, min_useless_max_usefull_sort, max_usefull_min_useless_sort, max_potential_min_useless_sort, min_useless_max_potential_sort, usefull_proportion_sort, potential_proportion_sort]
    sort_names = ["best_portion_sort", "min_useless_max_usefull_sort", "max_usefull_min_useless_sort", "max_potential_min_useless_sort", "min_useless_max_potential_sort", "usefull_proportion_sort", "potential_proportion_sort"]
    results = []
    for s in range(len(sorts)):
        results = results + [quality_assignment(leaves, tasks, dim, lambda leaves, task, dim: unique_heur(sorts[s], leaves, task, dim))]
        results = results + [quality_assignment(leaves, tasks, dim, lambda leaves, task, dim: greedy_heur(sorts[s], leaves, task, dim))]
    return [sort_names, results]


def make(leaves, tasks, dim):
    sorts = [best_portion_sort, min_useless_max_usefull_sort, max_usefull_min_useless_sort, max_potential_min_useless_sort, min_useless_max_potential_sort, usefull_proportion_sort, potential_proportion_sort]
    sorts_name = ["best_portion_sort", "min_useless_max_usefull_sort", "max_usefull_min_useless_sort", "max_potential_min_useless_sort", "min_useless_max_potential_sort", "usefull_proportion_sort", "potential_proportion_sort"]
    print("unique : ")
    for i in range(len(sorts)):
        print(sorts_name[i]+ " : " + str(quality_assignment(leaves, tasks, dim, lambda leaves, task, dim: unique_heur(sorts[i], leaves, task, dim))))
    print("greedy : ")
    for i in range(len(sorts)):
        print(sorts_name[i] +" : "+ str(quality_assignment(leaves, tasks, dim, lambda leaves, task, dim : greedy_heur(sorts[i], leaves, task, dim))))




