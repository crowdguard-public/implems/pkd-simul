from scipy.stats import nbinom
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

epsilon = 0.1
nusers = 10000

p = 1 - numpy.exp(-epsilon)

print("p = " + str(p))


def noise():
    lnp = sum(nbinom.rvs(1, p, 0, nusers))
    lnm = sum(nbinom.rvs(1, p, 0, nusers))
    return lnp - lnm


noises = [noise() for i in range(100000)]

sns.distplot(noises)
plt.savefig("noise.png")
