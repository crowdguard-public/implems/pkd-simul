import csv
import random


def import_csv(path, totalnumber):
    """
    Import a workers CSV file as a array of workers
    :param path: workers CSV file path
    :param totalnumber: Final number of users. Taken randomly in the list if there is more than totalnumber or return the raw workers set if there is less than totalnumber
    :return: An array of workers (array of skills level)
    """
    # Array of workers
    workers = []
    # Current worker skills
    current = []
    # Tags mapping
    skills_map = {}
    # Tags mapping index
    skills_map_index = 0

    # Current worker csv id
    userid = None
    # Number of imported workers
    i = 0

    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile)

        for row in reader:
            user = int(row[0])
            tag = int(row[1])
            level = float(row[2])

            # New user detected, add the old one
            if userid != user:
                if userid:
                    workers.append(current)
                    i += 1
                current = [0.] * len(skills_map)
                userid = user

            # Add the skill to the user
            index = skills_map.get(tag)
            if index == None:
                if i == 0:
                    skills_map[tag] = skills_map_index
                    index = skills_map_index
                    current.append(0)
                    skills_map_index += 1
                else:
                    print("ERROR NEW TAG")
                    exit(-42) # Not supposed to discover new tags at this point
            current[index] = level

        workers.append(current)

    if len(workers) > totalnumber:
        workers = random.sample(workers, totalnumber)

    return workers


if __name__ == '__main__':
    workers = import_csv('./ratiouv2.csv', 10000)

    print(len(workers))
    print(workers[:10])
