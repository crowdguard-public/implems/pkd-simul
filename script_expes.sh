#!/bin/bash

#Chemins à changer si besoin

if [ $# -eq 0 ]
then
	MAIN_PATH='main.py'
	KDEXPE='./expe'
	IGRIDA=0
else
	MAIN_PATH='/udd/'$USER'/Documents/crowdguard/ppcrowd/kdtrees/prog/python/main_ter.py'
	KDEXPE='/temp_dd/igrida-fs1/'$USER'/SCRATCH/expes_kdtrees'
	IGRIDA=1
fi

DATE=`date +%Y-%m-%d-%T`
OUT_FILE=$KDEXPE/$DATE.csv

echo "résultats condensés;nombre de dimensions;nombre d'utilisateur·ices;profondeur max;nombre de bins;epsilon;ration de eps pour les counts;modélisation des utilisateur·ices;impression de l'arbre;coalition;nombre de tâches;format de la tâche;erreur relative moyenne;erreur moyenne(quad);erreur moyenne(lin);ecart-type de l'erreur;max tasks for one leaf;tasks with no users (unpredicted)" > $OUT_FILE

#Variable principale, qui fait varier les paramètres

REPRODUCTIONS=1 # 20

echo "script commencé"
#Variable secondaire, pour reproduire l'expérience
for j in `seq 1 $REPRODUCTIONS`
do
    # 10 tags, unif
	python3 $MAIN_PATH True 10 10000 4 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 6 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 8 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &

	# 10 tags one_spe
	python3 $MAIN_PATH True 10 10000 4 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 6 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 8 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &

	# 10 tags csv
	python3 $MAIN_PATH True 10 10000 4 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_10.csv >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 6 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_10.csv >> $OUT_FILE &
	python3 $MAIN_PATH True 10 10000 8 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_10.csv >> $OUT_FILE &

	# 5 tags unif
	#python3 $MAIN_PATH True 5 10000 4 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 6 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 8 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 10 10 0.1 0.7 unif False 1 1000 random >> $OUT_FILE &

	# 5 tags one_spe
	#python3 $MAIN_PATH True 5 10000 4 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 6 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 8 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 10 10 0.1 0.7 one_spe False 1 1000 one_spe >> $OUT_FILE &

	# 5 tags csv
	#python3 $MAIN_PATH True 5 10000 4 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_5.csv >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 6 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_5.csv >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 8 10 0.1 0.7 csv False 1 1000 one_spe noisy noisy_hist ./ratiouv_5.csv >> $OUT_FILE &
	#python3 $MAIN_PATH True 5 10000 10 10 0.1 0.7 csv False 1 1000 random noisy noisy_hist ./ratiouv_5.csv >> $OUT_FILE &
done
wait
echo "fini"
